import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Index'
import Employee from '../components/Employee'
import Example from '../components/ExampleComponent'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/employee',
      name: 'Employee',
      component: Employee
    }, {
      path: '/example',
      name: 'Example',
      component: Example
    }
  ]
})
