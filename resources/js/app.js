require('./bootstrap');
window.Vue = require('vue');
// window.VueRouter = require('vue-router').default;
// window.VueAxios = require('vue-axios').default;
// window.Axios = require('axios').default;
import router from './router/index.js'
import BootstrapVue from 'bootstrap-vue'
let AppLayout = require('./components/App.vue')

Vue.use(BootstrapVue)
// Vue.use(VueRouter,VueAxios,Axios);

// const Header = Vue.component('header-component', require('./components/skins/Header.vue'));
// const Footer = Vue.component('footer-component', require('./components/skins/Footer.vue'));

new Vue(
  Vue.util.extend({
  	router
    },
    AppLayout
  )
).$mount('#app');
