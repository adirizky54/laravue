<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Employe;

class EmployeTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        Employe::truncate();

        foreach(range(1,200) as $a) {
            Employe::create([
                'name' => $faker->name,
                'address' => $faker->address,
                'phone' => $faker->phoneNumber
            ]);
        }
    }
}
