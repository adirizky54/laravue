<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employe;

class EmployeeController extends Controller
{
    public function getData(){
        $employe = Employe::paginate(20);

        return response()->json([
            'model' => $employe
        ]);
    }
}
